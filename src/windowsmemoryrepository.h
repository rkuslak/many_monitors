#pragma once

#ifdef _WIN32
#include "windows.h"
#include <cstdint>
#include <mutex>

#include "repositories/memoryrepository.h"

class WindowsMemoryRepository : public MemoryRepository {
  public:
    WindowsMemoryRepository() {}

    MEMORYSIZE_T GetMemoryTotal();
    MEMORYSIZE_T GetMemoryUsed();
    MEMORYSIZE_T GetSwapTotal();
    MEMORYSIZE_T GetSwapUsed();
    void UpdateSystemStatus() {}

  private:
    void UpdateSystemStats();

    MEMORYSTATUSEX memInfo;
};

#endif
