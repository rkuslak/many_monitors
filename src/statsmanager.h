/*
   Many Monitors - A system vitals monitor
   Copyright (C) 2019 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <atomic>
#include <cstdint>
#include <thread>
#include <vector>

#include "repositories/cpurepository.h"
#include "repositories/memoryrepository.h"

struct CPUPercentage {
  std::chrono::steady_clock::time_point reading_time;
  uint_fast8_t percentage;
};

class StatsManager {
public:
  StatsManager(const std::chrono::milliseconds update_interval,
               std::unique_ptr<CPURepository> &&cpu_repo,
               std::unique_ptr<MemoryRepository> &&memory_repo,
               const MemorySize memory_size, const uint32_t max_cpu_records);
  ~StatsManager();
  StatsManager(const StatsManager &mgr) { std::move(this); }

  const CPU_T GetCPUPercentageUsed() const;
  const CPU_T GetCPUCores() const;
  const CPU_T GetCPUFrequencyAverage() const;
  std::vector<uint8_t> GetCurrentCPUPercentages() const;
  std::vector<uint_fast8_t> GetCPUPercentages() const;

  const MEMORYSIZE_T GetMemoryUsed() const;
  const MEMORYSIZE_T GetMemoryFree() const;
  const MEMORYSIZE_T GetMemoryTotal() const;
  const uint_fast8_t GetMemoryUsedPercentage() const;
  std::string GetMemoryUsedString();
  std::string GetMemoryTotalString();
  std::string GetSwapUsedString();
  std::string GetSwapTotalString();

  const MEMORYSIZE_T GetSwapUsed() const;
  const MEMORYSIZE_T GetSwapFree() const;
  const MEMORYSIZE_T GetSwapTotal() const;
  const uint_fast8_t GetSwapUsedPercentage() const;

  void Join();
  void RunUpdateThread();
  void Update();

private:
  std::mutex _update_mutex;

  std::unique_ptr<CPURepository> _cpu_repo;
  std::vector<CPUPercentage> _cpu_percentage_readings{0};
  uint32_t _cpu_percentage_readings_max{0};

  std::unique_ptr<MemoryRepository> _memory_repo;
  MemorySize _memory_size{MEGABYTE};

  std::thread _update_thread;
  std::chrono::milliseconds _update_interval;
  bool _cancelation_token{false};

  void AddCPUReading(const uint_fast8_t cpu_percent);
  void SetMaxCPUReadings(const uint32_t new_size);
};
