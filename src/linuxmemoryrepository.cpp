/*
   Many Monitors - A system vitals monitor
   Copyright (C) 2019 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifdef __unix__

#include <chrono>
#include <fstream>
#include <iostream>
#include <string>

#include "linuxmemoryrepository.h"

// Path leads to memory information on the ProcFS
const std::string MEMORY_PROC_PATH("/proc/meminfo");

LinuxMemoryRepository::LinuxMemoryRepository() { UpdateSystemStatus(); }

MEMORYSIZE_T LinuxMemoryRepository::GetMemoryTotal() { return _memory_total; }

MEMORYSIZE_T LinuxMemoryRepository::GetMemoryUsed() {
  return _memory_total - _memory_free;
}

MEMORYSIZE_T LinuxMemoryRepository::GetSwapTotal() { return _swap_total; }

MEMORYSIZE_T LinuxMemoryRepository::GetSwapUsed() {
  return _swap_total - _swap_free;
}

const uintmax_t parseMemoryString(const std::string &memory_string) {
  size_t next_pos = 0;
  uintmax_t result = std::stoull(memory_string, &next_pos, 10);

  // Attempt to normalize based off formatted extention remaining:
  if (next_pos < memory_string.length() &&
      memory_string.substr(next_pos) == " kB") {
    result *= 1024;
  }

  return result;
}

void LinuxMemoryRepository::UpdateSystemStatus() {
  std::lock_guard<std::mutex> lg(_update_mutex);
  std::ifstream memory_info_stream(MEMORY_PROC_PATH);
  std::string line;

  if (!memory_info_stream.is_open()) {
    std::cout << "Failed to open memory info file" << std::endl;
    return;
  }

  while (std::getline(memory_info_stream, line)) {
    int32_t split = line.find(':');
    if (split < 0) {
      continue;
    }
    auto token = line.substr(0, split++);

    if (token == "MemTotal") {
      _memory_total = parseMemoryString(line.substr(split));
      continue;
    } else if (token == "MemAvailable") {
      _memory_free = parseMemoryString(line.substr(split));
    } else if (token == "SwapTotal") {
      _swap_total = parseMemoryString(line.substr(split));
      continue;
    } else if (token == "SwapFree") {
      _swap_free = parseMemoryString(line.substr(split));
    }
  }
}

std::unique_ptr<MemoryRepository> CreateMemoryRepository() {
  return std::unique_ptr<MemoryRepository>(new LinuxMemoryRepository);
}
#endif
