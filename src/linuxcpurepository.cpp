/*
   Many Monitors - A system vitals monitor
   Copyright (C) 2019 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifdef __unix__

#include <fstream>
#include <iostream>
#include <sstream>

#include "linuxcpurepository.h"
#include "repositories/cpurepository.h"

// Path leads to the CPU status path under the ProcFS
const std::string STAT_PROC_PATH("/proc/stat");

// This path leads to the CPU relevent paths under StatFS on a Linux system
const std::string SYSFS_CPU_ROOT_PATH("/sys/devices/system/cpu/");

// reads in a line passed in the format expected from SysFS for "present" CPUs,
// parsing out CPU numbers and adding them to the passed vector.
void addRangeToVector(std::vector<uint8_t> &store, const std::string line) {
  // TODO: I feel like there is some error checkin' that needs to happen here.

  auto dash_index = line.find('-');
  if (dash_index > 0) {
    // We have a dash; parse both numbers and add as range of CPUs
    short start = std::stoi(line.substr(0, dash_index++));
    short end = std::stoi(line.substr(dash_index));

    while (start <= end) {
      store.push_back(start);
      start++;
    }
  } else {
    // No dash; expect single value
    store.push_back(std::stoi(line));
  }
}

// Returns a vector of the numbered CPUs listed by the SysFS for the system.
// Each entry should be viable as a enumerated CPU
void getAvailableCPUs(std::vector<uint8_t> &cpus) {
  std::ifstream processors(SYSFS_CPU_ROOT_PATH + "present");
  std::string line;

  if (!processors.is_open()) {
    std::cout << "Failed to open processors present status file: " << std::endl;
    return;
  }

  while (std::getline(processors, line)) {
    int_fast32_t idx = line.find(',');
    while (idx > 0) {
      addRangeToVector(cpus, line.substr(idx));
      idx++;
      line.erase(0, idx);
      idx = line.find(',');
    }
    addRangeToVector(cpus, line);
  }
}

LinuxCPURepository::LinuxCPURepository() {
  getAvailableCPUs(_cpu_cores);
  UpdateSystemStatus();
}

const CPU_T LinuxCPURepository::GetCPUCores() const {
  return _cpu_cores.size();
}

const CPU_T LinuxCPURepository::GetCPUPercentageUsed() const {
  return _cpu_usage_current;
}

const CPU_T LinuxCPURepository::GetCPUFrequency() const { return _cpu_freq; }

std::unique_ptr<CPURepository> CreateCPURepository() {
  return std::unique_ptr<LinuxCPURepository>(new LinuxCPURepository());
}

const int8_t LinuxCPURepository::GetCPUTemperature() const { return 0; }

uint16_t getCPUFrequencyAverage(std::vector<uint8_t>& cpu_cores) {
  // Parse SysFS entries for each CPU found, and combine for aggrigate CPU
  // Frequency
  size_t cpus_read = 0;
  uint16_t cpu_freqs = 0;
  for (auto idx = cpu_cores.begin(); idx != cpu_cores.end(); idx++) {
    std::string cpu_freq_path = "/sys/devices/system/cpu/cpu";
    cpu_freq_path += std::to_string(*idx);
    cpu_freq_path += "/cpufreq/scaling_cur_freq";
    std::ifstream cpu_freq(cpu_freq_path);
    if (!cpu_freq.is_open()) {
      std::cout << "Failed read from: " << cpu_freq_path << std::endl;
      continue;
    }
    int freq;
    cpu_freq >> freq;
    cpu_freqs += (freq + 500) / 1000;
    cpus_read++;
  }
  if (cpus_read > 0) {
    return cpu_freqs / cpus_read;
  }
  return 0;
}

void LinuxCPURepository::UpdateSystemStatus() {

  std::lock_guard<std::mutex> lg(_update_mutex);
  std::fstream cpu_stat_file(STAT_PROC_PATH, std::fstream::in);
  std::string line;

  // If attempt to read CPU usage information failed, return early
  if (!cpu_stat_file.is_open()) {
    std::cout << "Failed to open CPU status file" << std::endl;
    return;
  }

  _cpu_freq = getCPUFrequencyAverage(_cpu_cores);

  // Find CPU line, and parse current CPU usage if found:
  while (std::getline(cpu_stat_file, line)) {
    std::string token;
    std::stringstream line_stream(line);

    line_stream >> token;

    if (token == "cpu") {
      uint64_t user, nice, system, idle, io_wait, softirq, steal, guest,
          guest_nice;
      line_stream >> user >> nice >> system >> idle >> io_wait >> softirq >>
          steal >> guest >> guest_nice;

      uint64_t total_time = user + nice + system + io_wait + idle + softirq +
                            steal + guest + guest_nice;
      uint64_t active_time = total_time - idle - io_wait;

      total_time -= _previous_total_time;
      active_time -= _previous_active_time;
      _previous_active_time += active_time;
      _previous_total_time += total_time;

      if (total_time == 0) {
        // Potential race attempting to update same instant as another?
        std::cout << "WTF MATE" << std::endl;
        std::cout << "WTF MATE" << std::endl;
        std::cout << "WTF MATE" << std::endl;
        std::cout << "WTF MATE" << std::endl;
        std::cout << "WTF MATE" << std::endl;
        std::cout << "WTF MATE" << std::endl;
        std::cout << "WTF MATE" << std::endl;
        std::cout << "WTF MATE" << std::endl;
        std::cout << "WTF MATE" << std::endl;

        return;
      }
      _cpu_usage_current = (active_time * 100) / total_time;
    }
  }
}

#endif
