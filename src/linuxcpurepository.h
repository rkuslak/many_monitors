/*
   Many Monitors - A system vitals monitor
   Copyright (C) 2019 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once
#ifdef __unix__
#include <cstdint>
#include <mutex>
#include <vector>
#include <iostream>

#include "repositories/cpurepository.h"

class LinuxCPURepository : public CPURepository {
public:
  LinuxCPURepository();

  virtual const CPU_T GetCPUCores() const;
  virtual const CPU_T GetCPUPercentageUsed() const;
  virtual const int8_t GetCPUTemperature() const;
  virtual const CPU_T GetCPUFrequency() const;
  virtual void UpdateSystemStatus();

protected:
  std::vector<uint8_t> _cpu_cores{};
  uint8_t _cpu_slot_count{0};
  uint8_t _cpu_average_temperature{0};
  uint8_t _cpu_usage_current{0};
  uint16_t _cpu_freq{0};

  // Used to retain the previous values for CPU percentage calculations
  uint64_t _previous_total_time{0};
  uint64_t _previous_active_time{0};
};

#endif
