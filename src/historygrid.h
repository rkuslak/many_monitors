/*
   Many Monitors - A system vitals monitor
   Copyright (C) 2019 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <QtWidgets>

// The size of the board width around the grid
const uint_fast8_t BORDER_WIDTH = 2;
const uint_fast8_t MARGIN_WIDTH = BORDER_WIDTH * 2;

class HistoryGrid : public QWidget {
public:
  HistoryGrid(QWidget *parent = nullptr, QBrush grid_fill = QBrush(Qt::blue));

  void SetPoints(const std::vector<uint_fast8_t> percents);
  void SetGridBrush(const QBrush grid_fill_brush);

  QSize sizeHint() const override;
  QSize minimumSizeHint() const override;
  void paintEvent(QPaintEvent *event) override;

private:
  std::vector<QPoint> _history_points;
  QBrush _grid_fill_brush;

  uint32_t _desired_height{20};
  uint32_t _desired_width{50};
};
