/*
   Many Monitors - A system vitals monitor
   Copyright (C) 2019 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <chrono>
#include <cstdint>
#include <iomanip>
#include <iostream>

#include <QApplication>

#include "mainwindow.h"
#include "repositories/cpurepository.h"
#include "repositories/memoryrepository.h"
#include "statsmanager.h"

int main(int argc, char **argv) {
  const auto update_interval = std::chrono::seconds(2);

  auto stats_manager = std::shared_ptr<StatsManager>(
      new StatsManager(update_interval, CreateCPURepository(),
                       CreateMemoryRepository(), MemorySize::GIGABYTE, 30));

  QApplication app(argc, argv);
  MainWindow window(stats_manager);
  window.show();

  return app.exec();
}
