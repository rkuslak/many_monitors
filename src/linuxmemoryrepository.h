/*
   Many Monitors - A system vitals monitor
   Copyright (C) 2019 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once
#ifdef __unix__
#include <chrono>
#include <memory>
#include <mutex>

#include "repositories/memoryrepository.h"

using std::chrono::milliseconds;

class LinuxMemoryRepository : public MemoryRepository {
public:
  LinuxMemoryRepository();

  MEMORYSIZE_T GetMemoryTotal();
  MEMORYSIZE_T GetMemoryUsed();
  MEMORYSIZE_T GetSwapTotal();
  MEMORYSIZE_T GetSwapUsed();
  void UpdateSystemStatus();

private:
  uintmax_t _memory_total = 1;
  uintmax_t _memory_free = 0;
  uintmax_t _swap_total = 1;
  uintmax_t _swap_free = 0;
};

#endif
