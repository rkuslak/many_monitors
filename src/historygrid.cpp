/*
   Many Monitors - A system vitals monitor
   Copyright (C) 2019 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "historygrid.h"
#include <QtWidgets>
#include <iostream>

HistoryGrid::HistoryGrid(QWidget *parent, QBrush grid_fill)
    : QWidget(parent), _grid_fill_brush{grid_fill} {}

QSize HistoryGrid::sizeHint() const {
  return QSize(_desired_width, _desired_height);
}

QSize HistoryGrid::minimumSizeHint() const {
  return QSize(_desired_width, _desired_height);
}

void HistoryGrid::SetGridBrush(const QBrush grid_fill_brush) {
  _grid_fill_brush = grid_fill_brush;
}

// Updates the set of points acting as the values for the history grid.
void HistoryGrid::SetPoints(const std::vector<uint_fast8_t> percents) {
  uint16_t bottom = height() - BORDER_WIDTH;
  float height_ratio = (float)(bottom - BORDER_WIDTH) / 100;
  float width_ratio = (float)(width() - MARGIN_WIDTH) / (percents.size() - 1);

  // add points so we have a base on which the other points will appear:
  _history_points.clear();
  _history_points.push_back(QPoint(width() - BORDER_WIDTH, bottom));
  _history_points.push_back(QPoint(BORDER_WIDTH, bottom));

  // Position all points provided on the grid:
  for (auto index = 0; index < percents.size(); index++) {
    uint_fast16_t point_height = bottom - (percents[index] * height_ratio);
    uint_fast16_t point_width = BORDER_WIDTH + (index * width_ratio);
    _history_points.push_back(QPoint(point_width, point_height));
  }

  update();
}

// Handles the repaint request event by completely redrawing the grid.
void HistoryGrid::paintEvent(QPaintEvent *event) {
  auto x = width();
  auto y = height();

  QPainter painter(this);
  painter.setRenderHint(QPainter::Antialiasing, true);

  // Draw background:
  QPainterPath dark_path;
  dark_path.moveTo(0, 0);
  dark_path.lineTo(width(), height());
  dark_path.lineTo(width(), 0);

  QPainterPath light_path;
  light_path.moveTo(0, 0);
  light_path.lineTo(width(), height());
  light_path.lineTo(0, height());

  painter.fillPath(dark_path, Qt::gray);
  painter.fillPath(light_path, Qt::lightGray);
  painter.fillRect(BORDER_WIDTH, BORDER_WIDTH, width() - (MARGIN_WIDTH),
                   height() - (MARGIN_WIDTH), QColor(10, 10, 10));

  // If we have data points draw graph:
  if (_history_points.size() >= 1) {
    auto pen_color = _grid_fill_brush.color().lighter(120);
    painter.setPen(QPen(pen_color, 2, Qt::SolidLine));
    painter.setBrush(_grid_fill_brush);
    painter.drawPolygon(&_history_points[0], _history_points.size());
  }
}
