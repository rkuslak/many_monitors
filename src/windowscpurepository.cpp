/*
   Many Monitors - A system vitals monitor
   Copyright (C) 2019 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifdef _WIN32
#include "windows.h"

#include <chrono>
#include <iostream>
#include <string>

#include "repositories/cpurepository.h"
#include "windowscpurepository.h"

extern "C" {
// #include <powerbase.h>
#include <powrprof.h>
#pragma comment(lib, "powrprof.lib")
}

const CPU_T WindowsCPURepository::GetCPUCores() const {
  SYSTEM_INFO info;
  GetSystemInfo(&info);
  return static_cast<CPU_T>(info.dwNumberOfProcessors);
}

uint64_t ConvertFileTimeToUInt64(const FILETIME &time) {
  return ((uint64_t)time.dwHighDateTime << 32) | ((uint64_t)time.dwLowDateTime);
  ULARGE_INTEGER converter;

  converter.HighPart = time.dwLowDateTime;
  converter.LowPart = time.dwHighDateTime;

  return converter.QuadPart;
}

const CPU_T WindowsCPURepository::GetCPUPercentageUsed() const {
  return _cpu_used_percentage;
}

std::unique_ptr<CPURepository> CreateCPURepository() {
  return std::unique_ptr<WindowsCPURepository>(new WindowsCPURepository());
}

void WindowsCPURepository::UpdateSystemStatus() {
  static uint64_t previous_total_ticks = 0;
  static uint64_t previous_used_ticks = 0;

  FILETIME idle_time, kernel_time, user_time;
  if (!GetSystemTimes(&idle_time, &kernel_time, &user_time)) {
    std::cout << "Failed to read CPU status!" << std::endl;
    return;
  }

  uint64_t total_ticks =
      ConvertFileTimeToUInt64(user_time) + ConvertFileTimeToUInt64(kernel_time);
  uint64_t used_ticks = total_ticks - ConvertFileTimeToUInt64(idle_time);

  total_ticks -= previous_total_ticks;
  used_ticks -= previous_used_ticks;
  previous_total_ticks += total_ticks;
  previous_used_ticks += used_ticks;

  _cpu_used_percentage = (used_ticks * 100) / total_ticks;
}

typedef struct _PROCESSOR_POWER_INFORMATION {
  ULONG Number;
  ULONG MaxMhz;
  ULONG CurrentMhz;
  ULONG MhzLimit;
  ULONG MaxIdleState;
  ULONG CurrentIdleState;
} PROCESSOR_POWER_INFORMATION, *PPROCESSOR_POWER_INFORMATION;

const CPU_T WindowsCPURepository::GetCPUFrequency() const {
  PROCESSOR_POWER_INFORMATION info;
  auto status = CallNtPowerInformation(ProcessorInformation, nullptr, 0, &info,
                                       sizeof info);
  return info.CurrentMhz / (1024 * 1024);
}

#endif
