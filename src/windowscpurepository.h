/*
   Many Monitors - A system vitals monitor
   Copyright (C) 2019 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once
#ifdef _WIN32
#include <cstdint>
#include <thread>

#include "repositories/cpurepository.h"

class WindowsCPURepository : public CPURepository {
public:
  WindowsCPURepository() {}

  virtual const CPU_T GetCPUCores() const;
  virtual const CPU_T GetCPUPercentageUsed() const;
  virtual const int8_t GetCPUTemperature() const { return 0; }
  virtual const CPU_T GetCPUFrequency() const;
  void UpdateSystemStatus();

private:
  CPU_T _cpu_used_percentage;
};
#endif
