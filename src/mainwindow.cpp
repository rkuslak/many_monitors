/*
   Many Monitors - A system vitals monitor
   Copyright (C) 2019 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QtCore>
#include <QtWidgets>

#include <iostream>

#include "mainwindow.h"
#include "statsmanager.h"
#include "historygrid.h"

MainWindow::MainWindow(std::shared_ptr<StatsManager> statsmgr,
                       QWidget *parent)
    : QMainWindow{parent}, _statsmgr(statsmgr) {
  window.setLayout(&layout);

  /* setWindowFlags(Qt::WindowStaysOnBottomHint | Qt::FramelessWindowHint |
   * Qt::Tool); */
  setWindowFlags(Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
  setWindowTitle("Memory Test");

  setCentralWidget(&window);
  QFont labels_font("Sans", 8, QFont::Bold);
  QFont values_font("Sans", 8, QFont::ExtraLight);

  _cpu_freq.setFont(values_font);
  _cpu_percent.setFont(values_font);
  _swap_current_used.setFont(values_font);
  _swap_percent.setFont(values_font);
  _memory_current_used.setFont(values_font);
  _memory_percent.setFont(values_font);

  _cpu_percent_label.setFont(labels_font);
  _cpu_freq_label.setFont(labels_font);
  _memory_label.setFont(labels_font);
  _swap_label.setFont(labels_font);

  layout.addWidget(&_cpu_percent_grid);
  layout.addWidget(&_cpu_percent_label);
  layout.addWidget(&_cpu_percent);

  layout.addWidget(&_cpu_freq_label);
  layout.addWidget(&_cpu_freq);

  layout.addWidget(&_memory_label);
  layout.addWidget(&_memory_percent);
  layout.addWidget(&_memory_current_used);

  layout.addWidget(&_swap_label);
  layout.addWidget(&_swap_percent);
  layout.addWidget(&_swap_current_used);

  layout.addWidget(&_close_button);

  UpdateLabels();
  QObject::connect(&_update_timer, &QTimer::timeout, this,
                   &MainWindow::OnUpdate);
  _update_timer.start(500);

  connect(&_close_button, SIGNAL(clicked()), this, SLOT(close()));
}

void MainWindow::UpdateLabels() {
  QString cpu_percent =
      QString::number(_statsmgr->GetCPUPercentageUsed()) + "%";
  _cpu_percent.setText(cpu_percent);
  auto memory_string = _statsmgr->GetMemoryUsedString() + " / " +
                       _statsmgr->GetMemoryTotalString();
  auto swap_string =
      _statsmgr->GetSwapUsedString() + " / " + _statsmgr->GetSwapTotalString();

  QString memory_percent =
      QString::number(_statsmgr->GetMemoryUsedPercentage()) + "%";
  _memory_percent.setText(memory_percent);
  _memory_current_used.setText(QString::fromStdString(memory_string));

  QString swap_percent =
      QString::number(_statsmgr->GetSwapUsedPercentage()) + "%";
  _swap_percent.setText(swap_percent);
  _swap_current_used.setText(QString::fromStdString(swap_string));

  _cpu_freq.setText(QString::number(_statsmgr->GetCPUFrequencyAverage()));
}

void MainWindow::OnUpdate() {
  auto readings = _statsmgr->GetCPUPercentages();

  UpdateLabels();
  _cpu_percent_grid.SetPoints(readings);
}
