#ifdef _WIN32
#include <chrono>
#include <ctime>

#include "repositories/memoryrepository.h"
#include "windows.h"
#include "windowsmemoryrepository.h"

MEMORYSIZE_T WindowsMemoryRepository::GetMemoryTotal() {
  UpdateSystemStats();
  auto result = memInfo.ullTotalPhys;

  return result;
}

MEMORYSIZE_T WindowsMemoryRepository::GetMemoryUsed() {
  return memInfo.ullTotalPhys - memInfo.ullAvailPhys;
}

MEMORYSIZE_T WindowsMemoryRepository::GetSwapTotal() {
  return memInfo.ullTotalPageFile;
}

MEMORYSIZE_T WindowsMemoryRepository::GetSwapUsed() {
  return 0 - memInfo.ullAvailPageFile + memInfo.ullTotalPageFile;
}

void WindowsMemoryRepository::UpdateSystemStats() {
  std::lock_guard<std::mutex> lg(_update_mutex);
  memInfo.dwLength = sizeof(MEMORYSTATUSEX);
  GlobalMemoryStatusEx(&memInfo);
}

std::unique_ptr<MemoryRepository> CreateMemoryRepository() {
  return std::unique_ptr<MemoryRepository>(new WindowsMemoryRepository);
}
#endif
