/*
   Many Monitors - A system vitals monitor
   Copyright (C) 2019 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once
#include "historygrid.h"
#include "statsmanager.h"
#include <QtGui>
#include <QtWidgets>

class MainWindow : public QMainWindow {
public:
  MainWindow(std::shared_ptr<StatsManager> statsmgr,
      QWidget *parent = nullptr
      );

  void UpdateLabels();
  void OnUpdate();

protected:
  std::shared_ptr<StatsManager> _statsmgr;
  QWidget window;
  QHBoxLayout layout;
  QLabel _cpu_percent;
  QLabel _cpu_percent_label{"CPU %"};

  QLabel _memory_percent;
  QLabel _memory_current_used;
  QLabel _memory_label{"Memory"};

  QLabel _swap_percent;
  QLabel _swap_current_used;
  QLabel _swap_label{"Swap"};

  QLabel _cpu_freq;
  QLabel _cpu_freq_label{"CPU Freq"};
  HistoryGrid _cpu_percent_grid;

  QPushButton _close_button{"&Close"};

  QTimer _update_timer;
};
