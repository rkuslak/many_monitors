/*
   Many Monitors - A system vitals monitor
   Copyright (C) 2019 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "statsmanager.h"
#include <iostream>
#include <string>
#include <QtWidgets>

// Initializes a new instance of the StatsManager holding a pointer to the
// repository instances provided, and begins a new background thread to update
// stats on a timed interval based on the passed update_interval. Memory stats
// reported will be based on the MemorySize passed when generating string
// instances.
StatsManager::StatsManager(const std::chrono::milliseconds update_interval,
                           std::unique_ptr<CPURepository> &&cpu_repo,
                           std::unique_ptr<MemoryRepository> &&memory_repo,
                           const MemorySize memory_size,
                           const uint32_t max_cpu_records) {
  _cpu_repo = move(cpu_repo);
  _update_interval = update_interval;
  _memory_repo = move(memory_repo);
  _memory_size = memory_size;
  _cancelation_token = false;
  SetMaxCPUReadings(max_cpu_records);

  {
    std::lock_guard<std::mutex> lg(_update_mutex);
    _memory_repo->UpdateSystemStatus();
    _cpu_repo->UpdateSystemStatus();
  }

  _update_thread = std::thread(&StatsManager::RunUpdateThread, this);
}

// Releases the pointers provided to the repository instances, and forcably
// closes and joins the background update thread.
StatsManager::~StatsManager() {
  _cancelation_token = true;

  if (_update_thread.joinable()) {
    _update_thread.join();
  }
  _cpu_repo.reset();
  _memory_repo.reset();
}

// Updates the maximum amount of CPU readings to retain, dropping any instances
// no longer capable of holding in the new size, and adding 0 valued instances
// when growing.
void StatsManager::SetMaxCPUReadings(const uint32_t new_size) {
  std::lock_guard<std::mutex> lg(_update_mutex);
  if (new_size == _cpu_percentage_readings_max) {
    return;
  }

  // If size is smaller, move end-most readings to the front of the vector
  if (new_size < _cpu_percentage_readings_max) {
    auto read_iter = _cpu_percentage_readings.begin();
    auto write_iter = _cpu_percentage_readings.begin() +
                      (_cpu_percentage_readings_max - new_size);
    while (read_iter < _cpu_percentage_readings.end()) {
      std::swap(*read_iter, *write_iter);
      read_iter++;
      write_iter++;
    }
  }

  _cpu_percentage_readings.resize(new_size);

  // If size is larger, move current readings to the end of the vector
  if (new_size > _cpu_percentage_readings_max) {
    auto write_iter = _cpu_percentage_readings.begin();
    write_iter += new_size - _cpu_percentage_readings_max;
    for (auto read_iter = _cpu_percentage_readings.begin();
         read_iter <
         _cpu_percentage_readings.begin() + _cpu_percentage_readings_max;
         read_iter++) {
      std::swap(*read_iter, *write_iter);
      write_iter++;
    }
  }

  _cpu_percentage_readings_max = new_size;
}

const CPU_T StatsManager::GetCPUPercentageUsed() const {
  auto used = _cpu_repo->GetCPUPercentageUsed();
  return used;
}

const CPU_T StatsManager::GetCPUFrequencyAverage() const {
  return _cpu_repo->GetCPUFrequency();
}
std::string formatMemoryString(MEMORYSIZE_T memory_used,
                               MemorySize memory_size) {
  if (memory_size <= KILOBYTE) {
    return std::to_string(memory_used);
  }

  std::string memory_string =
      std::to_string(((float)memory_used / memory_size) + 0.005);
  int32_t idx = memory_string.find('.');
  if (idx == -1) {
    memory_string += ".00";
  } else {
    memory_string += "00";
    memory_string = memory_string.substr(0, idx + 3);
  }
  switch (memory_size) {
  case GIGABYTE:
    memory_string += " gb";
    break;
  case MEGABYTE:
    memory_string += " mb";
    break;
  case KILOBYTE:
    memory_string += " kb";
    break;
  default:
    break;
  }
  return memory_string;
}

std::string StatsManager::GetMemoryUsedString() {
  return formatMemoryString(_memory_repo->GetMemoryUsed(), _memory_size);
}

std::string StatsManager::GetMemoryTotalString() {
  return formatMemoryString(_memory_repo->GetMemoryTotal(), _memory_size);
}

std::string StatsManager::GetSwapUsedString() {
  return formatMemoryString(_memory_repo->GetSwapUsed(), _memory_size);
}

std::string StatsManager::GetSwapTotalString() {
  return formatMemoryString(_memory_repo->GetSwapTotal(), _memory_size);
}

std::vector<uint8_t> StatsManager::GetCurrentCPUPercentages() const {
  std::vector<uint8_t> result{};

  for (auto percent = _cpu_percentage_readings.begin();
       percent != _cpu_percentage_readings.end(); percent++) {
    result.push_back(percent->percentage);
  }
  return result;
}

const CPU_T StatsManager::GetCPUCores() const {
  return _cpu_repo->GetCPUCores();
}

const MEMORYSIZE_T StatsManager::GetMemoryUsed() const {
  return _memory_repo->GetMemoryUsed();
}

const MEMORYSIZE_T StatsManager::GetMemoryFree() const {
  return _memory_repo->GetMemoryTotal() - _memory_repo->GetMemoryUsed();
}

const MEMORYSIZE_T StatsManager::GetMemoryTotal() const {
  return _memory_repo->GetMemoryTotal();
}

const uint_fast8_t StatsManager::GetMemoryUsedPercentage() const {
  return (_memory_repo->GetMemoryUsed() * 100) / _memory_repo->GetMemoryTotal();
}

const MEMORYSIZE_T StatsManager::GetSwapUsed() const {
  return _memory_repo->GetSwapUsed();
}

const MEMORYSIZE_T StatsManager::GetSwapFree() const {
  return _memory_repo->GetSwapTotal() - _memory_repo->GetMemoryUsed();
}

const MEMORYSIZE_T StatsManager::GetSwapTotal() const {
  return _memory_repo->GetSwapTotal();
}

const uint_fast8_t StatsManager::GetSwapUsedPercentage() const {
  return (_memory_repo->GetSwapUsed() * 100) / _memory_repo->GetSwapTotal();
}

void StatsManager::Update() {
  {
    std::lock_guard<std::mutex> lg(_update_mutex);
    _memory_repo->UpdateSystemStatus();
    _cpu_repo->UpdateSystemStatus();
  }

  // Add record of CPU readings
  AddCPUReading(_cpu_repo->GetCPUPercentageUsed());
}

// Adds a new reading to the _cpu_percentage_readings vector, pushing all
// current readings back 1
void StatsManager::AddCPUReading(const uint_fast8_t cpu_percent) {
  std::lock_guard<std::mutex> lg(_update_mutex);
  auto last_reading = _cpu_percentage_readings.end();
  last_reading--;
  for (auto reading = _cpu_percentage_readings.begin(); reading != last_reading;
       reading++) {
    auto next_reading = reading;
    next_reading++;
    std::swap(*reading, *next_reading);
  }

  *last_reading = CPUPercentage{std::chrono::steady_clock::now(), cpu_percent};
}

std::vector<uint_fast8_t> StatsManager::GetCPUPercentages() const {
  auto results = std::vector<uint_fast8_t>{};
  for (auto reading = _cpu_percentage_readings.begin(); reading != _cpu_percentage_readings.end(); reading++){
    results.push_back(reading->percentage);
  }
  return results;
}

void StatsManager::RunUpdateThread() {
  using std::chrono::steady_clock;

  auto last_update = steady_clock::now();

  while (!_cancelation_token) {
    if ((steady_clock::now() - last_update) > _update_interval) {
      Update();
      while ((steady_clock::now() - last_update) > _update_interval) {
        last_update += _update_interval;
      }
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
  }
}
