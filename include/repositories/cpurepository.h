#pragma once

#include <chrono>
#include <cstdint>
#include <memory>
#include <mutex>

typedef uint_least16_t CPU_T;

class CPURepository {
public:
  virtual ~CPURepository() {}
  virtual const CPU_T GetCPUCores() const = 0;
  virtual const CPU_T GetCPUPercentageUsed() const = 0;

  virtual const int8_t GetCPUTemperature() const = 0;

  virtual const CPU_T GetCPUFrequency() const = 0;

  virtual void UpdateSystemStatus() = 0;

protected:
  std::mutex _update_mutex;
};

std::unique_ptr<CPURepository> CreateCPURepository();
