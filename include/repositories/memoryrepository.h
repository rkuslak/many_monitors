#pragma once

#include <chrono>
#include <memory>
#include <mutex>

typedef uint_least64_t MEMORYSIZE_T;

enum MemorySize {
  BIT = 1,
  /* BYTE = 8, */
  KILOBYTE = 1024,
  MEGABYTE = 1024 * 1024,
  GIGABYTE = 1024 * 1024 * 1024,
};

class MemoryRepository {
public:
  virtual ~MemoryRepository() {}
  virtual MEMORYSIZE_T GetMemoryTotal() = 0;
  virtual MEMORYSIZE_T GetMemoryUsed() = 0;
  virtual MEMORYSIZE_T GetSwapTotal() = 0;
  virtual MEMORYSIZE_T GetSwapUsed() = 0;

  virtual void UpdateSystemStatus() = 0;

protected:
  std::mutex _update_mutex;
};

std::unique_ptr<MemoryRepository> CreateMemoryRepository();
